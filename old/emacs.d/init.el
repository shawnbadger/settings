;;; init.el

;; Remove some GUI elements
(when (fboundp 'menu-bar-mode) (menu-bar-mode -1))
(when (fboundp 'tool-bar-mode) (tool-bar-mode -1))
(when (fboundp 'scroll-bar-mode) (scroll-bar-mode -1))

(setq inhibit-startup-screen t)
(setq require-final-newline t)

;; (Useful command: M-x list-packages)
(require 'package)
(add-to-list 'package-archives
             '("melpa" . "http://melpa.milkbox.net/packages/"))
(add-to-list 'package-archives
             '("org" . "http://orgmode.org/elpa/"))
(package-initialize)

(load "~/.emacs.d/my-loadpackages.el")
(load "~/.emacs.d/my-loadkeyboard.el")

;; Smooth Scrolling
(setq scroll-margin 5
      scroll-conservatively 9999
      scroll-step 1)

;; Start Maximized
(custom-set-variables
 '(initial-frame-alist (quote ((fullscreen . maximized)))))

;; GDB Setup
(setq gdb-many-windows t)

;; Set exec path using $PATH
(defun set-exec-path-from-shell-PATH ()
  (let ((path-from-shell (replace-regexp-in-string
                          "[ \t\n]*$"
                          ""
                          (shell-command-to-string "$SHELL --login -i -c 'echo $PATH'"))))
    (setenv "PATH" path-from-shell)
    (setq eshell-path-env path-from-shell) ; for eshell users
    (setq exec-path (split-string path-from-shell path-separator))))

(when window-system (set-exec-path-from-shell-PATH))
