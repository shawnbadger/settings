;; keyboard.el

;; map multiple states at once (courtesy of Michael Markert;
;; http://permalink.gmane.org/gmane.emacs.vim-emulation/1674)
(defun set-in-all-evil-states (key def &optional maps)
  (unless maps
    (setq maps (list evil-normal-state-map
                     evil-visual-state-map
                     evil-insert-state-map
                     evil-emacs-state-map
		     evil-motion-state-map)))
  (while maps
    (define-key (pop maps) key def)))


(defun set-in-all-evil-states-but-insert (key def)
  (set-in-all-evil-states key def (list evil-normal-state-map
				   evil-visual-state-map
				   evil-emacs-state-map
				   evil-motion-state-map)))

;; up/down/left/right
(set-in-all-evil-states-but-insert "t" 'evil-next-line)
(set-in-all-evil-states-but-insert "T" 'evil-join)
(set-in-all-evil-states-but-insert "j" 'evil-forward-word-begin)
(set-in-all-evil-states-but-insert "J" 'evil-forward-WORD-begin)

;; map : to ;
(define-key evil-motion-state-map ";" 'evil-ex)
