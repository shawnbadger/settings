; my-loadpackages.el

(load "~/.emacs.d/my-packages.el")

;; Evil Mode
(setq evil-want-C-u-scroll t)
(evil-mode t)
(setq evil-emacs-state-cursor '("red" box))
(setq evil-normal-state-cursor '("green" box))
(setq evil-visual-state-cursor '("orange" box))
(setq evil-insert-state-cursor '("red" bar))
(setq evil-replace-state-cursor '("red" bar))
(setq evil-operator-state-cursor '("red" hollow))

;; Theme
(load-theme 'monokai' t)

;; Company Mode (autocompletion)
(setq company-idle-delay 0)
(add-hook 'after-init-hook 'global-company-mode)

;; Go Mode
(setq gofmt-command "goimports")
(add-hook 'go-mode-hook (lambda ()
    (set (make-local-variable 'company-backends) '(company-go))
    (company-mode)
    (go-eldoc-setup)
    (add-hook 'before-save-hook 'gofmt-before-save)
    (local-set-key (kbd "C-c m") 'gofmt)
    (local-set-key (kbd "M-.") 'godef-jump)))



