; my-packages.el

(defvar required-packages
  '(
    evil
    monokai-theme
    go-mode
    company-go
    go-eldoc
    go-errcheck
    ; magit
    ; yasnippet
  )
  "a list of packages to install (if not present) at launch.")


(require 'cl)

; method to check if all packages are installed
(defun packages-installed-p ()
  (loop for p in required-packages
        when (not (package-installed-p p)) do (return nil)
        finally (return t)))

; if not all packages are installed, check one by one and install the missing ones.
(unless (packages-installed-p)
  ; check for new packages (package versions)
  (message "%s" "Emacs is now refreshing its package database...")
  (package-refresh-contents)
  (message "%s" " done.")
  ; install the missing packages
  (dolist (p required-packages)
    (when (not (package-installed-p p))
      (package-install p))))


