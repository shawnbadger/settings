;; projectile.el

(use-package projectile
  :ensure t
  :config
  (projectile-mode))
