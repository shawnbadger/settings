;; theme.el

(use-package monokai-theme
  :ensure t
  :config (load-theme 'monokai' t))
