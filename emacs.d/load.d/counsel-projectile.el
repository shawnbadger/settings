;; counsel-projectile.el

(use-package counsel-projectile
  :ensure t
  :config
  (counsel-projectile-mode))
