;; which-key.el

(use-package which-key
  :ensure t
  :config
  (which-key-mode)
  (which-key-setup-minibuffer)
  (setq which-key-idle-delay 0.)
  (setq which-key-max-description-length 27)
  (setq which-key-add-column-padding 0)
  (setq which-key-max-display-columns nil))
