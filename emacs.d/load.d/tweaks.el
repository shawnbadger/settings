;; tweaks.el

; remove some GUI elements
(when (fboundp 'menu-bar-mode) (menu-bar-mode -1))
(when (fboundp 'tool-bar-mode) (tool-bar-mode -1))
(when (fboundp 'scroll-bar-mode) (scroll-bar-mode -1))

(setq inhibit-startup-screen t)
(setq require-final-newline t)

; smooth scrolling
(setq scroll-margin 5
      scroll-conservatively 9999
      scroll-step 1)

; start maximized
(custom-set-variables
	'(initial-frame-alist (quote ((fullscreen . maximized)))))

; set exec path using $PATH
(defun set-exec-path-from-shell-PATH ()
    (let ((path-from-shell (replace-regexp-in-string
	                           "[ \t\n]*$"
	                           ""
                               (shell-command-to-string "$SHELL --login -i -c 'echo $PATH'"))))
	     (setenv "PATH" path-from-shell)
		 (setq eshell-path-env path-from-shell) ; for eshell users
		 (setq exec-path (split-string path-from-shell path-separator))))

(when window-system (set-exec-path-from-shell-PATH))

