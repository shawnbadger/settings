;; evil.el

(use-package evil
  :ensure t
  :init
  (setq evil-want-C-u-scroll t)
  (setq evil-shift-width 4)
  (setq evil-search-module 'evil-search)

  :config
  (evil-mode t)
  (setq evil-emacs-state-cursor '("red" box))
  (setq evil-motion-state-cursor '("orange" box))
  (setq evil-normal-state-cursor '("green" box))
  (setq evil-visual-state-cursor '("orange" box))
  (setq evil-insert-state-cursor '("red" bar))
  (setq evil-replace-state-cursor '("red" bar))
  (setq evil-operator-state-cursor '("red" hollow))

  (general-define-key
    :states 'motion
    ";" 'evil-ex
    ":" 'evil-repeat-find-char)
)
