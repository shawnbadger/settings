;; counsel.el

(use-package counsel
  :ensure t
  :requires (ivy)
  :config
  (counsel-mode t))
