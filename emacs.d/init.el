;; init.el

; Add package repositories
(require 'package)
(add-to-list 'package-archives '("melpa" . "http://melpa.milkbox.net/packages/"))
(add-to-list 'package-archives '("org" . "http://orgmode.org/elpa/"))
(package-initialize)

(setq
  backup-by-copying t
  backup-directory-alist '(("." . "~/.emacs.d/backup/"))
  delete-old-versions t
  kept-new-versions 3
  kept-old-versions 1
  version-control t)

; Load my packages
(load "~/.emacs.d/packages.el")

; Load my keybindings
(load "~/.emacs.d/keybindings.el")

; Load generated custom variables
(setq custom-file "~/.emacs.d/custom.el")
(load custom-file)
