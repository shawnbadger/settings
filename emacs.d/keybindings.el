;; keybindings.el

;; Custom leader key
(general-create-definer leader-define-key
  :prefix "SPC"
  :non-normal-prefix "M-SPC")

;; Custom leader keybindings
(leader-define-key
  :init (general-def :states '(normal motion) "SPC" nil)
  :states '(normal visual motion)
  "SPC" '(counsel-M-x       :which-key "counsel-M-x")
  "f"   '(counsel-find-file :which-key "counsel-find-file"))

;; Global keybindings
(general-define-key
  "\C-s" 'swiper)

;; Normal keybindings
(general-define-key
  :states '(normal visual motion)
  "/" 'swiper)

;; Rapid key sequences for insert mode
(use-package key-seq
  :ensure t
  :config
  (key-seq-define evil-insert-state-map "qs" #'save-buffer))

;; Escape from insert mode using a key sequence
(use-package evil-escape
  :ensure t
  :config
  (evil-escape-mode)
  (setq-default evil-escape-key-sequence "hc"))
