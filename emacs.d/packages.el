;; packages.el

; Packages to install at launch, if not present
(defvar required-packages
  '(
    use-package
    load-dir
   ))

(require 'cl)

; Checks if all packages in required-packages are installed
(defun packages-installed-p ()
  (loop for p in required-packages
	when (not (package-installed-p p)) do (return nil)
	finally (return t)))

; Install any packages from required-packages that are not already installed
(unless (packages-installed-p)
  (message "%s" "Emacs is now refreshing its package database...")
  (package-refresh-contents)
  (message "%s" " done.")
  (dolist (p required-packages)
    (when (not (package-installed-p p))
	  (package-install p))))

; Load *.el files in ~/emacs.d/load.d/
(require 'load-dir)
(custom-set-variables '(load-dirs t))
(load-dirs)

